package main

import (
	"fmt"
	"net/http"
	"os"
)

func status(w http.ResponseWriter, _ *http.Request) {
	w.WriteHeader(http.StatusOK)
}

func hello(w http.ResponseWriter, _ *http.Request) {
	hostname, _ := os.Hostname()

	fmt.Fprintf(w, "👋 from %s\n", hostname)
}

func main() {
	http.HandleFunc("/", hello)
	http.HandleFunc("/status", status)

	fmt.Println("Listening on port 8090")
	http.ListenAndServe(":8090", nil)
}
