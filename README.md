# Gitpod playground

[![Open in Gitpod](https://gitpod.io/button/open-in-gitpod.svg)](https://gitpod.io/#https://github.com/andrew-farries/gitpod-playground)

A playground for experimenting with Gitpod.

## Notes when onboarding

* Install the browser extension!
* You have `root` and can `sudo` extra packages into a workspace.
* `gitpod/workspace-full` is the default base image for workspaces.
  * This base image has docker in inside it.
  * So you can run and build docker images inside the workspace.
  * [How does this docker in docker stuff work? There is a talk by Chris about it somewhere].
* Docker compose is also supported. Not sure what this means in practice.
* Contents of the `/workspace/` dir are preserved between workspace restarts; not so other locations!
* What does it mean to 'download' a workspace? (from the workspace list in the UI).
* Can pin a workspace to prevent it from being deleted; they are kept forever.
* If you open a Gitpod workspace from an issue URL, Gitpod automatically creates a branch for you!
* `gp snapshot` is interesting. It creates a snapshot but doesn't create a new workspace immediately, until you open the URL.
* `gitpod.yml` also has a `before` field that will run before either the `init` or `command` entries.
* If you change the `gitpod.yml` file you need to start a new workspace to see the changes. Just restarting an existing one based on an earlier commit won't work.
* Can set git config in `gitpod.yml` too but it's probably best done using the new dotfiles functionality.
* How is gitpod's browser vscode remembering that I have the vim extension installed even though it isn't mentioned in the `gitpod.yml`?
